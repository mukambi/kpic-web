<footer class="footer ">
    <p class="text-muted text-center text-md-center">
        Copyright © {{ date('Y') }} <a href="#" target="_blank">{{ config('app.name') }}</a>. All rights
        reserved</p>
</footer>
